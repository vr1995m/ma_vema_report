%% MRCNN results
i1 = imread('rd_64.png');
i2 = imread('rd_119.png');
i3 = imread('rd_213.png');
i1_d = imread('rd_det64.png');
i2_d = imread('rd_det119.png');
i3_d = imread('rd_det213.png');
figure(1);
subplot(231)
imagesc(i1);
xlabel('Doppler');
ylabel('Range');
title('Original RD\_frame64');
axis off;
subplot(232)
imagesc(i2);
xlabel('Doppler');
ylabel('Range');
title('Original RD\_frame119');
axis off;
subplot(233)
imagesc(i3);
xlabel('Doppler');
ylabel('Range');
title('Original RD\_frame213');
axis off;
subplot(234)
imagesc(i1_d);
xlabel('Doppler');
ylabel('Range');
title('Predicted RD\_frame64');
axis off;
subplot(235)
imagesc(i2_d);
xlabel('Doppler');
ylabel('Range');
title('Predicted RD\_frame119');
axis off;
subplot(236)
imagesc(i3_d);
xlabel('Doppler');
ylabel('Range');
title('Predicted RD\_frame213');
axis off;
%% Performance metrics
loss = imread('plots/loss.png');
mb_loss= imread('plots/mrcnn_bbox_loss.png');
mc_loss = imread('plots/mrcnn_class_loss.png');
mm_loss = imread('plots/mrcnn_mask_loss.png');
figure(1);
subplot(221)
imshow(loss);
xlabel('Loss');
ylabel('Epochs');
title('General Loss');
subplot(222)
imagesc(mb_loss);
xlabel('Loss');
ylabel('Epochs');
title('MRCNN BBox Loss');
subplot(223)
imagesc(mc_loss);
xlabel('Loss');
ylabel('Epochs');
title('MRCNN Class Loss');
subplot(224)
imagesc(mm_loss);
xlabel('Loss');
ylabel('Epochs');
title('MRCNN Mask Loss');