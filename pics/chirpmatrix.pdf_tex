%% Creator: Inkscape inkscape 0.92.4, www.inkscape.org
%% PDF/EPS/PS + LaTeX output extension by Johan Engelen, 2010
%% Accompanies image file 'chirpmatrix.pdf' (pdf, eps, ps)
%%
%% To include the image in your LaTeX document, write
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics{<filename>.pdf}
%% To scale the image, write
%%   \def\svgwidth{<desired width>}
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics[width=<desired width>]{<filename>.pdf}
%%
%% Images with a different path to the parent latex file can
%% be accessed with the `import' package (which may need to be
%% installed) using
%%   \usepackage{import}
%% in the preamble, and then including the image with
%%   \import{<path to file>}{<filename>.pdf_tex}
%% Alternatively, one can specify
%%   \graphicspath{{<path to file>/}}
%% 
%% For more information, please see info/svg-inkscape on CTAN:
%%   http://tug.ctan.org/tex-archive/info/svg-inkscape
%%
\begingroup%
  \makeatletter%
  \providecommand\color[2][]{%
    \errmessage{(Inkscape) Color is used for the text in Inkscape, but the package 'color.sty' is not loaded}%
    \renewcommand\color[2][]{}%
  }%
  \providecommand\transparent[1]{%
    \errmessage{(Inkscape) Transparency is used (non-zero) for the text in Inkscape, but the package 'transparent.sty' is not loaded}%
    \renewcommand\transparent[1]{}%
  }%
  \providecommand\rotatebox[2]{#2}%
  \newcommand*\fsize{\dimexpr\f@size pt\relax}%
  \newcommand*\lineheight[1]{\fontsize{\fsize}{#1\fsize}\selectfont}%
  \ifx\svgwidth\undefined%
    \setlength{\unitlength}{656.53782161bp}%
    \ifx\svgscale\undefined%
      \relax%
    \else%
      \setlength{\unitlength}{\unitlength * \real{\svgscale}}%
    \fi%
  \else%
    \setlength{\unitlength}{\svgwidth}%
  \fi%
  \global\let\svgwidth\undefined%
  \global\let\svgscale\undefined%
  \makeatother%
  \begin{picture}(1,0.35186101)%
    \lineheight{1}%
    \setlength\tabcolsep{0pt}%
    \put(0,0){\includegraphics[width=\unitlength,page=1]{pics/chirpmatrix.pdf}}%
    \put(0.55409738,0.01779227){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}Time \, $t$\end{tabular}}}}%
    \put(0.39390458,-0.06832591){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\begin{minipage}{0.00897566\unitlength}\centering \end{minipage}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=2]{pics/chirpmatrix.pdf}}%
    \put(0.12190968,0.01111643){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$T_\text{r}$\end{tabular}}}}%
    \put(0.01428838,0.16594318){\color[rgb]{0,0,0}\rotatebox{90}{\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}Frequency \, $f$\end{tabular}}}}}%
    \put(0.33286697,0.33797425){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}(a)\end{tabular}}}}%
    \put(0.87228021,0.33797425){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}(b)\end{tabular}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=3]{pics/chirpmatrix.pdf}}%
    \put(0.97140083,0.01842591){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$N_c$\end{tabular}}}}%
    \put(0.69849065,0.06394686){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$N_s$\end{tabular}}}}%
    \put(0.86985283,0.01548077){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}Slow-Time\end{tabular}}}}%
    \put(0.70302272,0.20593782){\color[rgb]{0,0,0}\rotatebox{90}{\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}Fast-Time\end{tabular}}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=4]{pics/chirpmatrix.pdf}}%
    \put(0.12819407,0.1271382){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$f_\text{b}$\end{tabular}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=5]{pics/chirpmatrix.pdf}}%
    \put(0.08416454,0.21986863){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$f_\text{max}$\end{tabular}}}}%
    \put(0.01434437,0.0589389){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$f_\text{min}$\end{tabular}}}}%
    \put(0.22743595,0.00972008){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$t_\text{d}$\end{tabular}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=6]{pics/chirpmatrix.pdf}}%
    \put(0.53801831,0.31863704){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$T_\text{x}$ \, chirp\end{tabular}}}}%
    \put(0.53762414,0.29467433){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}$R_\text{x}$ \, chirp\end{tabular}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=7]{pics/chirpmatrix.pdf}}%
  \end{picture}%
\endgroup%
