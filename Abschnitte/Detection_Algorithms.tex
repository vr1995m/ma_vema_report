
\chapter{Detection Algorithms}
\label{sec:Detection_Algorithms}
\pagestyle{scrheadings}

	\section{State of the art}
    The major challenge in object detection is to decide if a peak in the spectrum corresponds to a
    potential target or not. Comparing the frequency spectrum to a fixed threshold value could work good for
    an ideal spectrum.
    In a real time scenario, the presence of noise with unknown power may cause many false alarms if the
    threshold value was chosen too low. Conversely, if it is set too high, fewer objects will be detected.
    Thereby, a new set of adaptive threshold detection namely Constant False Alarm Rate (CFAR) has been
    introduced to mitigate above problems.
    
    \subsection{Effect of Unknown Interference}
    The false alarm probability of an unknown interference modeled by square detector in a white Gaussian noise environment with unnormalized samples is given by Equation \eqref{eq4.1}.
    
    \begin{equation}\label{eq4.1}
    P_{fa} = e^{\frac{-T}{\beta^2}}
    \end{equation}
    
    The threshold $T$ is proportional to the noise power. Accurate setting of threshold requires appropriate
    knowledge of noise power $\beta^2$. This interference noise mostly is unknown and varies with different
    factors. Small changes in the interference power would lead to changes in threshold as a result in the
    false alarm probability. In order to avoid such a problem, a radar system designer prefers to have a
    constant false alarm rate. 
    
    \subsection{General Concept of CFAR}
    The detector uses range-Doppler processing as discussed in the sub-section \ref{subsec:RD Estimation}.
    It looks for target in each available sample. The cell to be tested in labelled as $x_i$ as shown in
    Figure \ref{fig:cfar} below.
    
	\begin{figure}[!ht]
		\centering
		\includegraphics[width=\linewidth]{pics/cfar.png}
		\caption{CFAR block diagram}
		\label{fig:cfar}
	\end{figure}
	
    The CFAR processing is based on two major assumptions
    \begin{itemize}
         \setlength{\itemsep}{0.1pt}
        \item Homogeneous Interference is exhibited between cells.
        \item Neighboring cells contain no targets
    \end{itemize}
    
    
\subsection{Cell Averaging CFAR}
Cell-averaging CFAR (CA-CFAR) is obtained by the maximum likelihood estimate resulting in the average of available samples. The threshold is a multiple of the estimated interference power. The principle of CA-CFAR algorithm is shown in the figure below. Averaging over the reference windows consisting of $N$ values, presents the background noise estimation of this CFAR algorithm.\\
 The cells adjacent to the cell under test are called the guard cells. The resultant averaging yield is defined as shown in the Equation
\eqref{eq4.2}

\begin{equation}\label{eq4.2}
    Z = {\frac{1}{N}}\sum_{i=1}^{N}X_i
    \end{equation}
    
    \begin{figure}[!ht]
    \centering
  	\includegraphics[width=\linewidth]{pics/cacfar.png}
  	\caption{CA-CFAR \cite{cfar_blockdiag}}
  	\label{fig:cacfar}
	\end{figure}

	
\subsubsection{Calculation of the threshold T}
To get an adaptive threshold value for comparison, a scaling factor has to be found in order to properly scale the result of the averaging of the above obtained value of Z. For a given probability of false alarm $P_{fa}$ and a fixed window size $N$, a constant threshold $T$ is determined. The input of CFAR algorithms is exponentially distributed if a square law detector is used. 

    Assuming that the interference is independent identically distributed (i.i.d.), the probability density function (pdf) of a cell $x_i$ is given by \eqref{eq4.3}
    
    \begin{equation}\label{eq4.3}
    p_{x_i}(x_i) = {\frac{1}{\beta^2}}e^{\frac{-x_i}{\beta^2}}
    \end{equation}

Using the standard results from probability and Equation \eqref{eq4.4}, we can write the pdf of $z_i$ and $T$ as follows

	\begin{equation}\label{eq4.4}
    p_{z_i}(z_i) = \left({\frac{N}{\beta^2}}\right)e^{\frac{-N{z_i}}{\beta^2}} ,
    \hspace{0.2in}
    p_{T}(T) = {{\frac{N}{\beta^2}}^N}{\frac{(T^{N-1})}{(N - 1)!}e^{\frac{-NT}{\beta^2}}}
    \end{equation}

The $P_{fa}$ observed with the estimated threshold is $exp({\frac{N}{\beta^2}})$. As this value is a random variable, the expectation of the $P_{fa}$ is taken and then standard integrals is computed. After some algebraic manipulations as followed in \cite{radar_basics}, the final result is given by 

    \begin{equation}\label{eq4.5}
    P_{fa} = \left(1 + {\frac{T}{N}}\right)^{-N}
    \end{equation}
    
    The scaling factor T is constant for fixed values of $P_{fa}$ as well as N, and it can be therefore in prior calculated. With the multiplication of T, the determined average $Z$ is an adaptive threshold for object detection, which satisfies the specified probability of false alarm $P_{fa}$ for a given Additive gaussian white noise (AWGN) channel. The CA-CFAR detection outputs a boolean, stating if a cell exceeds the threshold or not.
    
    \subsubsection{Limitations of CA-CFAR}
    
    The detection procedure in CA-CFAR is not designed for multiple target detection. Other objects present in the reference window distort the noise estimation and increase the threshold value as a consequence. Therefore, an algorithm which can solve such a kind of scenario is discussed in the sub-section below. 


\subsection{Ordered Statistics CFAR}
In contrast to the CA-CFAR procedure, which uses all signal amplitudes in the reference window to determine a threshold, the Ordered Statistic CFAR (OS-CFAR) algorithm only selects a single amplitude value for estimation of the interference. The selection of the the amplitude is based on the rank-procedure. The reference window data samples are sorted in the ascending numerical order and stored into list called ordered list. This $k$th element of the ordered list is called $k$th ordered statistic. This ordered statistic is selected as representative of the interference level and a threshold is a multiple of it.

	\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{pics/oscfar.png}
	\caption{OS-CFAR \cite{cfar_blockdiag}}
	\label{fig:oscfar}
	\end{figure}

Here the important note is that threshold depends on all of the given data, since all of the samples are required to determine the $k$th largest of all the samples.

\begin{equation}\label{eq4.6}
P_{fa} = k \binom{N}{k}{\frac{(k-1)!(T_{OS}+N-k)!}{(T_{OS}+N)!}}
\end{equation}


The OS-CFAR algorithm comprises of a shift-register containing A storage cells, and the cell under test. The amplitudes are passed as an input to the sorting algorithm as described above. In this rank selection process, we select the $k$th ordered statistic multiplied with $\alpha$ and compare the input values to this threshold.

\subsubsection{Calculation of the threshold T}
At first, a suitable value for $k$ in the ordered statistic must be found. The factor $T$ can be calculated using the below Equation \eqref{eq4.6}. For a given probability of false alarm and $k$th value of an ordered-statistic array of exponentially distributed values \cite{radar_basics}. Using the plots from the Figure 3.3, the ordered statistic is chosen approximately as $k=3N/4$.

%cdvdfg Figure from MA Richards Book, Fundam of RD

\subsubsection{Limitations of OS-CFAR}
The limitation of Ordered statistic CFAR is the processing power. It requires high computing power due to the presence of sorting algorithm.
    

	

\subsection{Smallest of Cell Averaging CFAR}
Smallest-of Cell-averaging CFAR (SOCA-CFAR) also known as least of CA-CFAR. This technique is intended to combat masking effect casued by interference among CFAR reference cells.

	\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{pics/socacfar.png}
	\caption{SOCA-CFAR \cite{cfar_blockdiag}}
	\label{fig:socacfar}
	\end{figure}


In a N-cell SOCA method, the lead and lag windows are averaged separately to make two different estimates such as $\beta_1$ and $\beta_2$. Each of these beta-values are computed over $N/2$ reference cells. The threshold is then computed by finding the minimum of both the above estimates.

\begin{equation}\label{eq4.7}
P_{fa} = \frac{1}{2}\left(2 + \frac{\alpha_{SO}}{N/2}\right)^{-N/2} \left[\sum_{k=0}^{\frac{N}{2}-1}\binom{\frac{N}{2}-1+k}{k}{\left(2 + \frac{\alpha_{SO}}{N/2}\right)^{-k}}\right]
\end{equation}

The inference can be drawn as follows. If the interfering target is present in one of the two windows, it will raise the interference power estimate in that window. Thus, lesser the amplitude of two estimates, there is high probability that it is the representation of interference level. This level can be used to set the threshold in the detection.

\subsubsection{Limitations of SOCA-CFAR}
Thresholding is often ineffective. The reason for this is that the mean-value calculation inevitably raises the threshold in the neighbourhood of targets. Consequently, far spaced targets can mask each other, 
particularly if a large target is located close to a small or extended target. 

Furthermore, because of the minimum selection process, the SOCA threshold cannot immediately follow an abrupt rise or fall in clutter level, e.g. the front of a thunderstorm. Therefore the threshold will lead in time in case of a steep rise in clutter level and will lag in case of a steep fall. Both these effects can result in target losses.




\subsection{GOCA-CFAR}
Greatest-of Cell-averaging CFAR (GOCA-CFAR) also known as least of CA-CFAR. This technique is intended to combat masking effect casued by interference among CFAR reference cells as shown below.

    \begin{figure}[h]
    \centering
	\includegraphics[width=\linewidth]{pics/gocacfar.png}
	\caption{GOCA-CFAR \cite{cfar_blockdiag}}
	\label{fig:gocacfar}
	\end{figure}


In a N-cell GOCA method, the lead and lag windows are averaged separately to make two different estimates such as $\beta_1$ and $\beta_2$ as in the GOCA-CFAR technique . Each of these beta-values are computed over $N/2$ reference cells. The difference lies here where the threshold is then computed by finding the maximum of both the above estimates.

\begin{equation}\label{eq4.8}
P_{fa} = \frac{1}{2}\left(1 + \frac{\alpha_{GO}}{N/2}\right)^{-N/2} - \left(2 + \frac{\alpha_{GO}}{N/2}\right)^{-N/2} \left[\sum_{k=0}^{\frac{N}{2}-1}\binom{\frac{N}{2}-1+k}{k}{\left(2 + \frac{\alpha_{GO}}{N/2}\right)^{-k}}\right]
\end{equation}


GOCA used in nonhomogeneous areas makes that the false alarms drop but also the probability of detection is slightly reduced. Therefore, this technique shows a good advantage in transition areas of big power level difference but at the same time drops the sensitivity in homogeneous background noise situations.
    

\subsubsection{Limitations of GOCA-CFAR}
Thresholding is often ineffective. The reason for this is that the mean-value calculation inevitably raises the threshold in the neighbourhood of targets. Consequently, closely spaced targets can mask each other, 
particularly if a small target is located close to a large or extended target. 

Furthermore, because of the MAX-process, the GOCA threshold cannot immediately follow an abrupt rise or fall in clutter level, e.g. the front of a thunderstorm. Therefore the threshold will lead in time in case of a steep rise in clutter level and will lag in case of a steep fall. Both these effects can result in target losses.



\subsection{Cell Averaging Statistic Hofele CFAR}
The Cell Averaging Statistic Hofele CFAR (CASH-CFAR) comprises of a circuit of A sub-registers, each containing L storage cells. By means of a special maximum-minimum process, a clutter representative sum-value $S_l$, will be selected from A sum-values, $S_1$,...$S_A$. The threshold value is calculated based on this above maximum or minimum circuit. By means of a special maximum-minimum process, a clutter representative sum-value $S_l$, will be selected from A sum-values, $S_1$,...$S_A$. The threshold value is calculated based on this above maximum or minimum circuit.\\

	\begin{figure}[h]
	\centering
  	\includegraphics[width=\linewidth]{pics/cashcfar.png}
  	\caption{CASH-CFAR \cite{cfar_blockdiag}}
  	\label{fig:cashcfar}
	\end{figure}

\subsubsection{Limitations of CASH-CFAR}
The complexity of this algorithm occurs in terms of hardware complexity. The reason is due to the maximum-minimum circuit which needs registers for both maximum and minimum circuits separately.

	

\subsection{Effect of Algorithm Parameters}
Each CFAR algorithm basically has three parameters reference window size, guard size and threshold. To achieve the aimed objective, one should make three fundamental decisions regarding the parameters optimal for the algorithm.

First, it is necessary to select the size of the CFAR sliding window. This window moves around the entire detection area so that each cell has a chance to occupy the central cell of the window at a given moment. The central cell is always the one that is evaluated to decide whether a target exists.  

Lets take the the example of CA-CFAR to understand in detail. The CA-CFAR mechanism estimates the average size of the clutter by averaging the values of the window cells. Therefore, higher window sizes achieve a high precision in the estimated mean value, while smaller sizes are associated with a lower processing load. Based on the input image size, one must properly choose reference cells and guard cells that offer a good balance between gain and resource consumption. 

Secondly, the samples in the given data is an important factor. A very small number of samples leads to a poor estimate of the resulting optimal threshold $T$, while processing too many samples can slow down the execution of the algorithm considerably.  


Finally it had to be decided which $P_{fa}$ values should be considered in the detection procedure. It should be noted that a minimum $P_{fa}$ requires data with more sample point. It is important to understand that the increase in $T$ always benefits the $P_{fa}$, but also decreases the probability of detection. Therefore, choosing a too high $T$-value is not a reasonable option. The exact trade-off between $P_{fa}$ and $P_{d}$ is to be adjusted on a corresponding $T$ value without additional loss. The threshold that is too low will produce a very good $P_d$ because it will cause the threshold to remain low, which will result in the detection of most targets. However, this low threshold will also result in clutter samples of large size being classified as targets, which will result in a very poor $P_{fa}$ value. The opposite effect is achieved when a very high threshold value is selected.

	\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\linewidth]{pics/cfar_params.png}
	\caption{CFAR Threshold}
	\label{fig:cfar_threshold}
	\end{figure}



The Figure \ref{fig:cfar_threshold} above shows the variation of threshold with respect to targets and cluster. The data is generated using Rand stream from MATLAB with 200 training cells. The corresponding custom thresholds are $T_1=4.25$ (red line) and $T_2=12.35$ (blue line). This simple example is used only for illustration in the variation of threshold.

  



\section{Clustering}

\subsection{K-Means Clustering}
Consider a data set of N data points $(x^{i})_{1 \leq i \leq N}$ which live in the d-dimensional feature space. The primary goal is to partition the data set into K number of clusters. It can be formalized with the notion by ﬁrst introducing a set of n-dimensional vectors $\mu_k$ , where $k =1,...,K$, in which $\mu_k$ is the mean associated with the kth cluster. 
The goal is to ﬁnd an assignment of data points to clusters, as well as a set of vectors $\mu_k$, such that the sum of the squares of the distances of each data point to its closest vector $\mu_k$, is a minimum. The objective function $J$ represents the sum of square of distances of each point to its assigned vector $\mu_k$.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\linewidth]{pics/kcluster.png}
	\caption{K-Means Clustering}
	\label{fig:k-means}
\end{figure}


The objective function is to be minimzed by iteratively finding the values of $r_{nk}$ and $\mu_k$. The two parameters are estimated using Expectation-Maximization algorithm as discussed detail in \cite{pr_bible}. After applying the E-M algorithm we get the values in the Equation \eqref{eq4.9}. The $(a)$ and $(b)$ in the Figure \ref{fig:k-means} above represents a dataset before and after clustering.

\begin{equation}\label{eq4.9}
f(x) = r_{nk} =
\begin{cases}
1,& \text{if } k = \arg\max_{x} \norm{x_n - \mu_j}^2\\
0,              & \text{otherwise } 
\end{cases}
\end{equation}

\begin{equation}\label{eq4.10}
Z = {\frac{\sum_{n}r_{nk}x_n}{\sum_{n}r_{nk}}}
\end{equation}
	
The denominator in this expression is equal to the number of points assigned to cluster $k$, and so this result has a simple interpretation, namely set $μ_k$ equal to the mean of all of the data points $x_n$ assigned to cluster $k$. For this reason, the procedure is known as the $K$-means algorithm.
	
	
	
\subsection{DBSCAN}
	
	A category of Density-based clustering, DBSCAN stands for Density-based spatial clustering of applications with noise. The density in center based approach is estimated for a particular point in the dataset by counting the number of points within a specified radius, $\epsilon$ of that point \cite{clust_tech}.
	
	The density of any point will depend on the specified radius. The center-based approach to density allows us to classify a point as being
	
	\begin{itemize}
		\item Interior of a dense region (a core point)
		\item Edge of a dense region (a border point)
		\item Sparsely occupied region (a noise or background point)
	\end{itemize}
	
	\begin{figure}[h]
		\centering
		\includegraphics[width=0.5\linewidth]{pics/dbscan_pts.png}
		\caption{DBSCAN points \cite{dbscan}}
		\label{fig:dbscan_points}
	\end{figure}
	
	\textbf{Core points}: These points are in the interior of a density-based cluster. A point is a core point if the number of points within a given neighborhood around the point as determined by the distance function and a user-specified distance parameter, $\epsilon$, exceeds a certain threshold, MinPts, which is also a user-specified parameter.
	
	\textbf{Border points}: A border point is not a core point, but falls within the neighborhood of a core point. A border point can fall within the neighborhoods of several core points.
	
	\textbf{Noise points}: A noise point is any point that is neither a core point nor a border point.
	
	In the Figure \ref{fig:dbscan_points}, A is a core point, B is a border point and C is a noise point. With the knowledge of points in the density based dataset. The algorithm can be explained as follows. Any two core points that are close enough within a distance $\epsilon$ of one another are put in the same cluster. Likewise, any border point that is close enough to a core point is put in the same cluster as the core point. (Ties may need to be resolved if a border point is close to core points from different clusters). Noise points are discarded. The formal details are given in the algorithm in \ref{sec:Appendix}. This algorithm uses the same concepts and finds the clusters.
	
	DBSCAN can find many clusters that could not be found using K-means. However, DBSCAN has trouble with high-dimensional data because density is more difficult to define for such data. The problem lies in the computational complexity of finding nearest neighbours using region query. In this report, the method is used as the range-doppler of radar mostly doesnot have high problems with density, except in the situations of high noise. For such kind of situations, preprocessing of data is highly essential before performing clustering. \cite{dbscan}

		
\section{Neural Network Implementation}
\subsection{Implementation using U-Net}
\textbf{Structure of neural network}\\
A Fully Connected Network (U-Net) is implemented with following layers in order. It has 3 encoder blocks and one encoder-decoder bottleneck and 3 decoder blocks.
Each encoder block has 2 X ConvNet followed by a LeakyRELU. The output of the encoder block is then passed to Batch Normalization and then the output from the layer is passed to the Max Pooling layer. The output of the encoder block is then passed to a Concatenate layer with the input from encoder block. Similarly, the decoder has 2 X ConvNet followed by a LeakyRELU. The output of the above block is then concatenated with the encoder layers. At the final stage, the output from decoder is passed to the ConvNet with sigmoid activation to get the output class.

For training this network, Adam Optimizer is used with a learning rate of $1e-4$ and categorical focal loss as the loss function. The architecture used for the implementation is shown in Figure \ref{fig:unet} below.


\begin{figure}[h]
   	\centering
   	\includegraphics[width=0.55\linewidth]{pics/unet.pdf}
   	\caption{U-Net}
   	\label{fig:unet}
\end{figure}
    
The network also yields good results when trained using a binary cross-entropy loss function.

\subsection{Implementation using Mask R-CNN}
The implementation of Mask R-CNN is based on the implementation of matterport on git repository. As already discussed in the Section \ref{sec:Fundamentals}, the architecture of Mask R-CNN (regional convolutional neural network) is a two stage framework: the first stage scans the image and generates proposals(areas likely to contain an object). And the second stage classifies the proposals and generates bounding boxes and masks.

\begin{figure}[h]
	\centering
	\footnotesize
	\def\svgwidth{\textwidth}
	\input{pics/mrcnn_arch.pdf_tex}
	\caption{Proposed deep learning region based cnn architecture}
	\label{fig:mrcnn_architecture}
\end{figure}

The implementation can be explained on an higher level. Custom backbone is the trained convolutional network that is used for early detection of features. In this thesis, ResNet-50 architecture is used as a custom backbone. To improve the object detection with respect to feature scalability, Feature pyramid network is used in combination with the ResNet. The network has currently understanding of different features in the given input. The RPN comes into picture where anchors of aspect ratio (0.5,1,2) with 256 training anchors per image are chosen. The RPN scans over the backbone feature map generated earlier and generates two outputs for each anchor. The first output is the anchor class, bounding box parameter. The anchor class decides whether the object is in the box or not. The bounding box parameter helps in the refinement of the highest overlap score using the principle of non-maximum suppression. Once the anchors are correctly overlapped the regions of interest is given to the second stage.


The proposals given by the RPN are subjected to ROI pooling. This is similar to the RPN network but has the ability to find the deeper features of the image. The ROI pooling generates two outputs class and bounding box parameter. The ROI pooling performs resize and crop operations on feature map to limit it to a fixed size. This operation enables classification process without ambiguity. The mask branch is a convolutional network that takes the positive regions selected by the ROI classifier and generates masks for them. The generated masks are of low resolution. But they are soft masks, represented by float numbers, so they hold more details than binary masks. The small mask size helps keep the mask branch light. During training, scaling down the ground-truth masks to above resolution helps to compute the loss, and during inference, scaling up the predicted masks to the size of the ROI bounding box and which yields the final masks, one per object.

\subsubsection{Mask R-CNN Library}
Mask R-CNN tools created for the practical part of the thesis consist of two modules. This
implementation is based on a Python implementation of Mask R-CNN \cite{mrcnn_git} written by Waleed Abdulla from Matterport, Inc published their implementation under the MIT License.

The library consists of four Python modules.
\begin{itemize}
	\item config.py: The configuration file for the model
	\item model.py: The core of the Mask R-CNN model. It builds up the model
	\item target.py: The target or custom named file contains the different parameters and calls to the functions for defined in the model
	\item utils.py: Utilities for the model
\end{itemize}


