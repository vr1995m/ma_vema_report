/* Das ist eine Testdatei.
 * Mehr gibt es nicht zu sagen
 */

#include <stdio.h>
#include <stdint.h>

int main(void) {
	uint8_t variable1 = 1;
	uint8_t variable2 = 2;
	uint8_t hilfsvariable;
	
	printf("Wir tauschen zwei Variablen!\r\n");
	
	hilfsvariable = variable1;
	variable1 = variable2;
	variable2 = hilfsvariable;
	
	printf("Wir haben getauscht!\r\n");
	
	return 0;
}