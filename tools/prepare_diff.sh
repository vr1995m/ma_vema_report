#!/bin/bash
cd masterarbeit && git pull && cd ..
cp -r masterarbeit korrektur-$(date +%Y-%m-%d)
mkdir revision-$(date +%Y-%m-%d)
cp -r masterarbeit revision-$(date +%Y-%m-%d) && rm revision-$(date +%Y-%m-%d)/Thesis.tex
