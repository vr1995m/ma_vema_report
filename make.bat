@ECHO OFF

REM make.bat for windoof

if "%1" == "" goto all


if "%1" == "clean" (
	goto end
)

if "%1" == "thesis" (
	lualatex --shell-escape Thesis.tex
	goto end
)


:all

:end
