filename=Thesis

all: thesis clean
thesis:
	lualatex -shell-escape ${filename}.tex && biber ${filename} && lualatex -shell-escape ${filename}.tex && makeindex ${filename}.idx && lualatex -shell-escape ${filename}.tex
clean:
	-cp ${filename}.pdf ../
	-rm ${filename}.pdf
	-rm ${filename}.log
	-rm ${filename}.toc
	-rm ${filename}.aux
	-rm ${filename}.out
	-rm ${filename}.idx
	-rm ${filename}.ind
	-rm ${filename}.ilg
	-rm ${filename}.lof
	-rm ${filename}.bbl
	-rm ${filename}.blg
	-rm ${filename}.nav
	-rm ${filename}.snm
	-rm ${filename}.auxlock
	-rm ${filename}.bcf
	-rm ${filename}.lot
	-rm ${filename}.run.xml
