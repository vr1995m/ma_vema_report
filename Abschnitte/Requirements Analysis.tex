
\chapter{Requirements Analysis}
	\label{sec:Requirements Analysis}
	\pagestyle{scrheadings}
	\section{Architecture}
	The architecture for the following thesis is described below in Figure \ref{fig:pipeline}. In Figure \ref{fig:pipeline}(a), the initial pre-processing is performed over the input signal and range-doppler maps are generated. This serves as an input to the traditional target detection using the principles of signal processing and as well as for the proposed deep learning architecture. \cite{autoencoder_paper}
	
	\begin{figure}[h]
		\centering
		\footnotesize
		\def\svgwidth{0.7\textwidth}
		\input{pics/pipeline.pdf_tex}
		\caption{(a) Traditional 2-stage target detection approach and (b) proposed deep learning based single stage target detection \cite{autoencoder_paper}}
		\label{fig:pipeline}
	\end{figure}
	

	\section{Data Preparation}
    The data is prepared with ideal software simulator and realistic simulation scenario for detection with automotive radar sensors. A radar sensor is placed on a setup bench and various objects are captured by the object from several different directions. As objects we selected a single instance of each of the following three different objects
    namely car, bicycle and pedestrians. Although these objects are visually easy to distinguish, they pose a greater challenge for detection algorithms when working in the radio frequency spectrum. Furthermore, when the scene is static and thus does not allow identifying the objects solely through the doppler spectrum. For radar, dynamic objects with different doppler spectra are easier to classify, due to their micro Doppler signature, but harder to record and annotate. For example, a moving person with arms and toes movement may have different signatures in the doppler domain. For every object, the range $R$, the relative radial velocity $v$, and the Direction of Arrival (DOA) $\theta$ are measured.
    
    In this thesis, two sources are used in order to generate input data. The first source is the radar simulator built in the MATLAB using drive scene scenario and signal processing toolbox and the another set of data is realtime measurements generated from radarbook. The software requirements are listed in the Chapter \ref{sec:Appendix}
    
    \subsection{Signal Parameters}
    The choice of the radar signal processing pipeline and the choice of the \ac{FMCW} parameters are important for accurately estimating the range and doppler components of the target. The parameters can also be altered and simulations with different specifications can be generated. The details of the chosen FMCW signal parameters are described in Table \ref{tab:radar_simulator_params}.
    
    \begin{table}[ht]
    	\begin{center}
    		\begin{tabular}{ l c }
    			\hline
    			Radar Parameters & Values \\
    			\hline
    			Cut-off frequency, $f_{0}$ & 77 GHz \\
    			Speed of light, $c$ & 3 x $10^8$ $m/s$ \\
    			Bandwidth (B) & 150MHz \\
    			Sampling frequency, $fs$ & 187 MHz \\
    			Number of samples/chirp, $N_s$ & 188 \\
    			Number of chirps/frame, $N_c$ & 128 \\
    			Number of Tx antennas, $N_{Tx}$ & 1 \\
    			Number of Rx antennas, $N_{Rx}$ & 1 \\
    			Chirp time, $T_c$ & 10 $\mu s$\\
    			\hline
    		\end{tabular}
    		\captionof{table}{FMCW signal parameters in the simulator} \label{tab:radar_simulator_params} 
    	\end{center}
    \end{table}
    
  	\vspace{-0.3in}
  	
	The details of the chosen FMCW signal parameters used in the real-time measurments are described in Table \ref{tab:radar_params}. However, the signal parameters are chosen to meet the hardware requirements.
    
    \begin{table}
    	\begin{center}
    		\begin{tabular}{ l c }
    			\hline
    			Radar Parameters & Values \\
    			\hline
    			Ramp start frequency, $f_{min}$ & 76 GHz \\
    			Ramp stop frequency, $f_{max}$ & 77 GHz  \\
    			Bandwidth (B) & 1GHz \\
    			Sampling frequency, $fs$ & 1 MHz \\
    			Number of samples/chirp, $N_s$ & 256 \\
    			Unambiguous range, $R_{max}$ & 50 m \\
    			Range resolution, $\delta_R$ & 15 cm \\
    			Chirp time, $T_c$ & 128 $\mu s$ \\
    			Chirp repetition time $T_{PRT}$ & 256 $\mu s$ \\
    			Number of chirps/frame, $N_c$ & 64 \\
    			Doppler resolution, $\delta_V$ & 0.15 m/s \\
    			Unambiguous velocity, $V_{max}$ & 3.9 m/s \\
    			Number of Tx antennas, $N_{Tx}$ & 1 \\
    			Number of Rx antennas, $N_{Rx}$ & 8 \\
    			Range FFT points, $NFFT_r$ & 1024 \\
    			Doppler FFT points, $NFFTd$ & 512 \\
    			\hline
    		\end{tabular}
    		\captionof{table}{FMCW signal parameters used for measurements} \label{tab:radar_params} 
    	\end{center}
    \end{table}

    \subsection{Data from Radar Simulator}
    \textbf{Driving Scenario Radar Simulation}\\
    The framework from Autonomous driving toolbox in MATLAB lets users create any driving scenarios with the DrivingScenarioDesigner App. It enables creation of raw radar data for the ego vehicles front radar. The Figure \ref{fig:drive_scene} shows a snapshot of the designer app. This feature enables users to directly obtain raw data without measurements.
    
    \begin{figure}[h]
    	\centering
    	\includegraphics[width=0.7\linewidth]{pics/drive_scene.png}
    	\caption{Driving Scenario Designer MATLAB}
    	\label{fig:drive_scene}
    \end{figure}
    %\vspace{-0.5in}

	\begin{itemize}
		\item Open the DrivingScenarioDesigner using MATLAB
		\item Create road and actor models using the interface.
		\item Configure sensors on the objects and use these sensors to simulate detections of actors and lane boundaries in the scenario
		\item The objects can be configured with parameters such as initial position, velocity and waypoints
		\item After the configuration, save the scenario and export it to your workspace
	\end{itemize}
   
   In addition to the above designer application, the signal processing for pre-processing is added into the simulator using user-defined functions. This enables us tho get the \ac{RD} maps with better information. As discussed in the beginning, this tool is easy to generate raw data for testing. The limitation of this tool is that it does not account many real-world parameters like noise from the hardware, environment and other factors. The ideality of the tool makes it easy but does not fit the real-time scenario.
    
    
    \subsection{Data from Real-time Measurements}
    In the previous subsection, we have seen that the synthetic data can be generated using a software tool. Due to its limitations, the following section explains the data generation with real-time radar sensor. The radarbook hardware in Figure \ref{fig:radarbook} is a versatile signal processing platform, developed to operate modern continuous wave radar systems with multiple receive and transmit channels in real-time. A standardized RF connector allows the control of different radar frontends from a single baseband platform.
    
     \begin{table}[ht]
    	\begin{center}
    		\begin{tabular}{ l c }
    			\hline
    			Features & Usage \\
    			\hline
    			MIMO Frontend  & Infineon 77 GHz \\
    			Signal processing & Rate reduction, FFT, Reconfigurable \\
    			Available frameworks & Sampling, Range-Doppler, Beamforming \\
    			Interfaces supported & LAN, WLAN, USB \\
    			\hline
    		\end{tabular}
    		\captionof{table}{Radarbook Features} \label{tab:radarbook_params} 
    	\end{center}
    \end{table}
    
    \begin{figure}
    	\centering
    	\includegraphics[width=0.3\linewidth]{pics/radarbook.png}
    	\caption{Radarbook}
    	\label{fig:radarbook}
    \end{figure}
    
    It operates over a frequency of 77 GHz with four transmit ($T_x$) and eight recieve ($R_x$) antennas. Morever it supports programming in $\#C$ and Octave/MATLAB. The features of the radarbook is listed in Table \ref{tab:radarbook_params}
    

	Additional details in detail about the radarbook is available in the \cite{radarbook}. The setup must be connected to power supply and to the Linux/Windows machine with USB. Once the device is installed and is in ready to use, the link cited above contains information about how to establish connection to the board and initiate the device. When the program is executed, the sensor emits the radiations and as a result the reflections as well as the targets in the test environment are generated over the doppler map. The pre-processing enables to eliminate some clutter and gives filtered output at the component end.


	\subsection{Instance Labeling}
	\ac{VIA} is a manual image annotation tool to define and describe regions in an image. The region shape can be rectangle, circle, ellipse, polygon, polyline, or a single point. Region descriptions can be plain text or a set of predefined options presented to manual annotators as checkbox, dropdown menu, radio buttons or image list. This tool \cite{vgg_annotate} supports bulk update of annotations corresponding to a large set of images.
	
	
	\begin{figure}[h]
		\centering
		\includegraphics[width=0.8\linewidth]{pics/via_annotate.png}
		\caption{VIA Annotator}
		\label{fig:via_annotate}
	\end{figure}
	

	The steps of creating labels manually is possible with the tool shown in Figure \ref{fig:via_annotate} are as follows.
	
	\begin{itemize}
		\item First click on 'Add Files' button, then upload your image from local machine
		\item The image is loaded and click on the desired shape with the given shape
		\item Hover the mouse with right click on the target and capture the path around it and click 'Enter'
		\item After the masks are created, click on File Menu -> Export Annotations as (.json)
	\end{itemize}

	The project created above can be saved at any point of time. This saved file can be loaded on the (.html). If further more masks are needed to be created, this can be done with the previously saved file. There is also the possibility of importing the saved (.json) file, make changes and export it again back to the required file. \cite{vgg_annotate}