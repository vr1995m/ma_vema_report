% generate x and y data
x=-10:0.1:10;
y=x.^2 + 1/2*x + 2;

% plotting
figure(1);
hold on;
xlabel('$x$');
ylabel('$f(x)$');
plot(x,y,'r')
legend('$f(x)=x^2 + \frac{1}{2}x + 2$');
matlab2tikz('../Abbildungen/matlabtikz.tikz', 'width', '0.5\textwidth', 'parseStrings', false);